#! /bin/bash

args=$@

if [ $@ ]; then
	alacritty --working-directory ~/bb/product/$@/dev -e nvim .
else
	alacritty --working-directory ~/bb/product/platform/dev -e nvim . &
	alacritty --working-directory ~/bb/product/advisor/dev -e nvim . &
	alacritty --working-directory ~/bb/product/company/dev -e nvim . &
	alacritty --working-directory ~/bb/product/investor/dev -e nvim . &
fi
