#! /bin/bash

cp ~/.bashrc ~/shells/.bashrc
cp ~/.bashrc-personal ~/shells/.bashrc-personal
cd ~/shells
echo '


#####  SHELLS  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/arco-setup-custom
rm -rf ~/arco-setup-custom/package_list.txt
pacman -Qq >>~/arco-setup-custom/package_list.txt
echo '


#####  ARCO_SETUP  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/qtile
echo '


#####  QTILE  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/alacritty
echo '


#####  ALACRITTY  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/starship
echo '


#####  STARSHIP  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/nvim
echo '

#####  NEOVIM  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/sddm
echo '

#####  SDDM  #####
'
~/shells/scripts/gitPush.sh push from update-script

cd ~/.config/dmscripts
echo '


#####  DM_SCRIPTS  #####
'
~/shells/scripts/gitPush.sh push from update-script
